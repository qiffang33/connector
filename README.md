# 本系统基于 IPAD 协议，使用说明如下
## 首先联系管理员获取 accesskey 来访问系统，一个 accesskey 可以支持多个微信同时在线。

## 请求二维码 API
```$xslt
API 请求
curl -XGET --header 'accesskey:18169647-f608-404a-936e-2e54efa23742' 'http://zpdemo.f3322.net:32725/api/v1/generateLoginQRCode'

返回消息内容如下：
{
    "code": 200,
    "data": {
        "Id": "81a6325a",
        "expiredTime": 290,
        "url": "http://zpdemo.f3322.net:32725/api/v1/image/81a6325a" // 点击该 URL 可以获取二维码图片
    },
    "text": "请务必记录 496b9e28, 后面的 API 调用需要用到该 ID。"
}
```
* accesskey 请找管理员申请，一个 accesskey 可以支持多个微信同时在线。
* 上面返回的 ID 是扫描该二维码的微信用户在本系统的标识，后面的接口会持续用到该 ID，请务必记录下来。


## 获取好友列表
```$xslt
API 请求
curl -XGET --header 'accesskey:18169647-f608-404a-936e-2e54efa23742' 'http://zpdemo.f3322.net:32725/api/v1/listFriends?id=81a6325a'

返回消息如下所示：
{
    "code": 200,
    "data": {
        "wxid_ekggt9065ez621": "方",
        ...
        ...
        ...
    },
    "text": "返回成功!"
}
``` 
* 该请求中的 id 为 `/api/v1/generateLoginQRCode` 返回的 ID

## 获取群列表
```$xslt
API 请求
curl -XGET --header 'accesskey:18169647-f608-404a-936e-2e54efa23742' 'http://zpdemo.f3322.net:32725/api/v1/listGroups?id=81a6325a'

返回消息如下所示：
{
    "code": 200,
    "data": {
        "15111963152@chatroom": "团购群",
        ...
        ...
    },
    "text": "返回成功!"
}

```
* 该请求中的 id 为 `/api/v1/generateLoginQRCode` 返回的 ID

## 发送文本消息
```$xslt
API 请求
curl -XPOST --header 'accesskey:18169647-f608-404a-936e-2e54efa23742' 'http://zpdemo.f3322.net:32725/api/v1/sendTextMsg?id=81a6325a' -d '{"toId": "wxid_e3ggtgy05ez631", "message":"test"}'
```
* 该请求中的 id 为 `/api/v1/generateLoginQRCode` 返回的 ID
* toId: 发送的微信 ID， 在 获取群列表 或者 获取好友列表 API 中找到需要发送的群/好友 微信 ID.
* message: 需要发送的文本内容。

## 发送图片消息
```$xslt
API 请求
curl -XPOST --header 'accesskey:18169647-f608-404a-936e-2e54efa23742' 'http://zpdemo.f3322.net:32725/api/v1/sendTextMsg?id=81a6325a' -d '{"toId": "wxid_e3ggtgy05ez631", "message":"test"}'
```
* 该请求中的 id 为 `/api/v1/generateLoginQRCode` 返回的 ID
* toId: 发送的微信 ID， 在 获取群列表 或者 获取好友列表 API 中找到需要发送的群/好友 微信 ID.
* message: 需要发送图片的 base64 内容，可以基于[该网站](https://tool.chinaz.com/tools/imgtobase) 将图片转换为 base64。

## 后续功能正在开发中。。。